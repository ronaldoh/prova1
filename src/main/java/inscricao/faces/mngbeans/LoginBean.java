/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import java.util.ArrayList;
import java.util.Date;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author ronaldo
 */
@Named(value = "loginBean")
@ApplicationScoped
public class LoginBean {

    private String usuario;
    private String senha;
    private String admin;

    private ArrayList<User> listaUsuarios = new ArrayList<>();

    /**
     * Creates a new instance of LoginBean
     */
    public LoginBean() {
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public ArrayList<User> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(ArrayList<User> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public String confirmaAction() {

        try {
            if (usuario.equals(senha)) {
                User chato = new User();
                chato.setLogin(usuario);
                chato.setDatahora(new Date());
                listaUsuarios.add(chato);
            }
        } catch (Exception e) {
        }
        if (admin.equals("false")) {
            return "cadastro";
        }
        return "admin";
    }
}
